FROM golang:1.12-alpine3.9

# Container arguments
ARG BUILD_DATE
ARG VERSION
ARG NAME="docker-cfssl-builder"
ARG DESCRIPTION="cfssl builder container"
ARG AUTHOR="Andrew Heberle"
ARG VCS_BASE_URL="https://gitlab.com/andrewheberle"
ARG VCS_REF
ARG ARCH="amd64"

# Download and compile cfssl
RUN apk add --no-cache --virtual .cfssl-builder-deps alpine-sdk git upx && \
	go get -u github.com/cloudflare/cfssl/cmd/... && \
    upx ${GOPATH}/bin/* && \
    mkdir -p /opt/cfssl/bin && \
    mkdir -p /opt/cfssl/etc && \
    mv ${GOPATH}/bin/* /opt/cfssl/bin/ && \
    apk del --no-cache .cfssl-builder-deps && \
    rm -rf ${GOPATH}/src/*

LABEL name="$NAME" \
      maintainer="$AUTHOR ($VCS_BASE_URL/$NAME)" \
      version="$VERSION" \
      architecture="$ARCH" \
      description="$DESCRIPTION" \
      maintainer="$AUTHOR ($VCS_BASE_URL/$NAME)" \
      org.label-schema.description="$DESCRIPTION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="$NAME" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.vcs-url="$VCS_BASE_URL/$NAME" \
      org.label-schema.version="$VERSION" \
      org.label-schema.schema-version="1.0"
